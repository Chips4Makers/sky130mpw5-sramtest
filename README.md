# PDKMaster based memory compiler test chip

## Description

This test chip contains a first version of an SRAM block with the in development single
port SRAM compiler. At the time the compiler can only generate a block with 512 words and
a column demultiplexer of 4 and there has to be exactly one we bit per 8 bits in each word.
For this tape-out a 512x8 block with one we pin has been used.

Current version of the SRAM cell is DRC compliant and not minimal area yet.

The top design consists of connecting in and out each of the SRAM signals using the caravan
analog frame. It has the following pin mapping:

| caravan pin   | signal    |
|---------------|-----------|
| io_analog[4]  | SRAM vdd  |
| io_analog[5]  | SRAM vss  |
| vssd1         | logic vss |
| vccd1         | logic vdd |
| io_in[0]      | SRAM we   |
| io_out[1]     | SRAM q[1] |
| io_out[2]     | SRAM q[2] |
| io_in[3]      | SRAM d[2] |
| io_in[4]      | SRAM d[3] |
| io_out[5]     | SRAM q[3] |
| io_out[6]     | SRAM q[4] |
| io_in[7]      | SRAM d[4] |
| io_in[8]      | SRAM d[5] |
| io_out[9]     | SRAM q[5] |
| io_out[10]    | SRAM q[6] |
| io_in[11]     | SRAM d[6] |
| io_in[12]     | SRAM d[7] |
| io_out[13]    | SRAM q[7] |
| io_in[14]     | SRAM a[0] |
| io_in[15]     | SRAM a[1] |
| io_in[16]     | SRAM a[2] |
| io_in[17]     | SRAM a[3] |
| io_in[18]     | SRAM a[4] |
| io_in[19]     | SRAM a[5] |
| io_in[20]     | SRAM a[6] |
| io_in[21]     | SRAM a[7] |
| io_in[22]     | SRAM a[8] |
| io_in[23]     | SRAM clk  |
| io_out[24]    | SRAM q[0] |
| io_in[25]     | SRAM d[0] |
| io_in[26]     | SRAM d[1] |

When `io_in[n]` is present in the table corresponding `io_out[n]` is connected to logic
zero and corresponding io_oeb[n] to logic one. When `io_out[n]` is present corresponding
`io_oeb[n]` is connected to logic zero.

One can also see that the SRAM block vss/vdd are connected to other pins than for the
logic vss/vdd. The logic consists of buffers and logic one and zero cells. As the SRAM is
not put in a deep nwell the SRAM vss and the logic vss will be connected to each other
through the (higher resistive) bulk of the die. Separate vdd pins though will allow to
measure the SRAM power consumption independently.

## Layout

Layout of the unconnected SRAM block:

![block](block.png)

Layout of the caravan top:

![top](top.png)

## Source

The top level is fully generated from python code in the `doitcode` subdirectory. pydoit is used to generate the design with the provided `dodo.py` file in the top directory. The code depends on some external modules that are assumed to be installed:

* [PDKMaster](https://gitlab.com/Chips4Makers/PDKMaster): python framework (under heavy development) to ease generation of circuits and corresponding DRC compliant layout. This based on a description of a technology with python souce code to allow easy porting to different technologies.
* [c4m-flexcell](https://gitlab.com/Chips4Makers/c4m-flexcell): a (currently minimal) standard cell library based on PDKMaster
* [c4m-flexmem](https://gitlab.com/Chips4Makers/c4m-flexmem): the source for the memory compiler
* [c4m-pdk-sky130](https://gitlab.com/Chips4Makers/c4m-pdk-sky130): the source for the PDKMaster based PDK for the Sky130 technology.  
  The workbook of the design of the DRC compliant 6T SRAM cell is available [in the repo](
      https://gitlab.com/Chips4Makers/c4m-pdk-sky130/-/blob/a73f6fae/notebooks/SRAMCellSP6T.ipynb
  )

As these code bases have still unstable APIs a mpw5 branch has been made in each of the repos with the state as for the tape-out.

## License

The resulting GDS files is released under [the LGPL 2.1 or later](https://spdx.org/licenses/LGPL-2.1-or-later.html) license. Some of the source code to generate the GDS is under [the GPL 2.0 or later](https://spdx.org/licenses/GPL-2.0-or-later.html) license.
