# SPDX-License-Identifier: LGPL-2.1-or-later
from os.path import dirname
from pathlib import Path

DOIT_CONFIG = {
    "default_tasks": ["gds"],
}


top_dir = Path(dirname(__file__))

code_dir = top_dir.joinpath("code")
code_py_files = tuple(code_dir.rglob("*.py"))

gds_dir = top_dir.joinpath("gds")
gds_empty_file = gds_dir.joinpath("user_analog_project_wrapper_empty.gds")
gds_out_file = gds_dir.joinpath("user_analog_project_wrapper.gds.gz")

def task_gds():
    """Generating GDS file with"""
    def run():
        from doitcode.generate import gen_gds

        gen_gds(name="_empty_", gds_out=gds_out_file, gds_empty=gds_empty_file)

    return {
        "file_dep": code_py_files,
        "targets": (gds_out_file,),
        "actions": (run,),
    }